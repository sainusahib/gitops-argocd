FROM scratch

LABEL maintaner="Sainudheen Sahib <sainusahib@gmail.com>"

COPY . .

EXPOSE 8080

CMD ["./main"]
